package it.unibo.coinquify.utils;

/**
 * This execpetion is throw in the case of an insert phone number is already
 * insered from another person.
 */
public class PhoneNumberPresentException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = 5767236089347423369L;

}
