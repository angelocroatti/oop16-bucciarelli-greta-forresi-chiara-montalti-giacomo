package it.unibo.coinquify.balance.model;


/**
 *
 *
 */
public interface Lending {
    /**
     * @return the debitor
     */
    Object getDebitor();
 
    /**
     * @return the description
     */
    String getDescription();
}
